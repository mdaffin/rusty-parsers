## Rust Parsers

Simple example parsers for various rust based parsing libraries.

| Library  | Source                                                                  | Links                                                 | Run                                |
| -------- | ----------------------------------------------------------------------- | ----------------------------------------------------- | ---------------------------------- |
| Nom:     | [srouce][nom-src]                                                       | [repo][nom-repo] [docs][nom-docs]                     | `cargo run --features lib_nom`     |
| Combine: | [srouce][combine-src]                                                   | [repo][combine-repo] [docs][combine-docs]             | `cargo run --features lib_combine` |
| Lalrpop: | [srouce][lalrpop-src] [grammer][lalrpop-grammer] [build][lalrpop-build] | [repo][lalrpop-repo] [book][lalrpop-book]             | `cargo run --features lib_lalrpop` |
| Pest:    | [srouce][pest-src] [grammer][pest-grammer]                              | [repo][pest-repo] [docs][pest-docs] [book][pest-book] | `cargo run --features lib_pest`    |

[nom-src]: https://gitlab.com/mdaffin/rusty-parsers/blob/master/src/lib_nom.rs
[nom-repo]: https://github.com/Geal/nom
[nom-docs]: https://docs.rs/nom/4.0.0/nom/
[combine-src]: https://gitlab.com/mdaffin/rusty-parsers/blob/master/src/lib_combine.rs
[combine-repo]: https://github.com/Marwes/combine
[combine-docs]: https://docs.rs/combine/3.5.2/combine/
[lalrpop-src]: https://gitlab.com/mdaffin/rusty-parsers/blob/master/src/lib_lalrpop.rs
[lalrpop-grammer]: https://gitlab.com/mdaffin/rusty-parsers/blob/master/src/part.lalrpop
[lalrpop-build]: https://gitlab.com/mdaffin/rusty-parsers/blob/master/build.rs
[lalrpop-repo]: https://github.com/lalrpop/lalrpop
[lalrpop-book]: http://lalrpop.github.io/lalrpop/
[pest-src]: https://gitlab.com/mdaffin/rusty-parsers/blob/master/src/lib_pest.rs
[pest-grammer]: https://gitlab.com/mdaffin/rusty-parsers/blob/master/src/part.pest
[pest-repo]: https://github.com/pest-parser/pest
[pest-book]: https://pest-parser.github.io/book/
[pest-docs]: https://docs.rs/pest/2.0.0/pest/
