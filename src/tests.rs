use super::{parse, Part};
use std::collections::HashMap;

macro_rules! test_case {
    ($test:ident, $input:expr, None) => {
        #[test]
        fn $test() {
            assert_eq!(parse($input).unwrap(), None);
        }
    };
    ($test:ident, $input:expr, Err) => {
        #[test]
        fn $test() {
            assert!(parse($input).is_err());
        }
    };
    ($test:ident, $input:expr, $name:expr, [ $($inner:expr => $outer:expr),* ]) => {
        #[test]
        fn $test() {
            #![allow(unused_mut)]
            let mut wires = HashMap::new();
            $(wires.insert($inner, $outer);)*
            let expected = Part {
                name: $name,
                wires: wires
            };
            assert_eq!(parse($input).unwrap().unwrap(), expected);
        }
    };
}

test_case!(simple_1, "Foo();", "Foo", []);
test_case!(simple_2, "Bar();", "Bar", []);
test_case!(simple_3, "FooBar();", "FooBar", []);
test_case!(single_wire_1, "Foo(a=b);", "Foo", ["a"=>"b"]);
test_case!(single_wire_2, "Foo(in=in);", "Foo", ["in"=>"in"]);
test_case!(single_wire_3, "Foo(input=input);", "Foo", ["input"=>"input"]);
test_case!(single_wire_4, "FOO(INPUT=INPUT);", "FOO", ["INPUT"=>"INPUT"]);
test_case!(single_wire_5, "Foo(a_b=c_d);", "Foo", ["a_b"=>"c_d"]);
test_case!(multi_wire_1, "Foo(a=a,b=b);", "Foo", ["a"=>"a", "b"=>"b"]);
test_case!(multi_wire_2, "Foo(a=z,b=y,c=x);", "Foo", ["a"=>"z", "b"=>"y", "c"=>"x"]);

test_case!(whitespace_1, " Foo ( a = z , b = y ) ; ", "Foo", ["a"=>"z", "b"=>"y"]);
test_case!(whitespace_2, "  Foo  (  a  =  z  ,  b  =  y  )  ;  ", "Foo", ["a"=>"z", "b"=>"y"]);
test_case!(whitespace_3, "\tFoo\t(\ta\t=\tz\t,\tb\t=\ty\t)\t;\t", "Foo", ["a"=>"z", "b"=>"y"]);
test_case!(whitespace_4,
       "\t\tFoo\t\t(\t\ta\t\t=\t\tz\t\t,\t\tb\t\t=\t\ty\t\t)\t\t;\t\t",
       "Foo", ["a"=>"z", "b"=>"y"]
    );
test_case!(whitespace_5, "\nFoo\n(\na\n=\nz\n,\nb\n=\ny\n)\n;\n", "Foo", ["a"=>"z", "b"=>"y"]);
test_case!(whitespace_6,
       "\n\nFoo\n\n(\n\na\n\n=\n\nz\n\n,\n\nb\n\n=\n\ny\n\n)\n\n;\n\n",
       "Foo", ["a"=>"z", "b"=>"y"]
    );

test_case!(partial_1, "F", None);
test_case!(partial_2, "Foo", None);
test_case!(partial_3, "Foo(", None);
test_case!(partial_4, "Foo(a", None);
test_case!(partial_5, "Foo(a=b", None);
test_case!(partial_6, "Foo(a=b,", None);
test_case!(partial_7, "Foo(a=b,c", None);
test_case!(partial_8, "Foo(a=b,c=", None);
test_case!(partial_9, "Foo(a=b,c=d", None);
test_case!(partial_10, "Foo(a=b,c=d)", None);
test_case!(partial_11, "Foo(a=b)", None);
test_case!(partial_12, "Foo()", None);

test_case!(error_1, "Foo(;", Err);
test_case!(error_2, "Foo(a;", Err);
test_case!(error_3, "Foo;", Err);
test_case!(error_4, "Foo()a;", Err);
