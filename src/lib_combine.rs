use combine::parser::char::{digit, letter, spaces};

use combine::parser::range::recognize;
use combine::{choice, sep_by, skip_many, token, Parser, skip_many1};

use super::Part;
use std::collections::HashMap;

pub fn parse(buffer: &str) -> Result<Option<Part>, String> {
    // Combine does support partial parsers but I have not yet figured them out
    if !buffer.trim_right().ends_with(";") {
        return Ok(None);
    }

    let t = |c| recognize(token(c).skip(spaces()));
    let chip_name =
        || recognize((skip_many1(letter()), skip_many((letter(), digit())))).skip(spaces());
    let wire_name = || {
        recognize((
            skip_many1(letter()),
            skip_many(choice((letter(), digit(), token('_')))),
        )).skip(spaces())
    };
    let wire_pair = || (wire_name(), t('='), wire_name()).map(|(inner, _, outer)| (inner, outer));
    let wire_map = || sep_by(wire_pair(), t(','));

    let part = || {
        spaces().with((chip_name(), t('('), wire_map(), t(')'), t(';')).map(
            |(name, _, wires, _, _): (_, _, Vec<_>, _, _)| {
                let wires = wires
                    .into_iter()
                    .fold(HashMap::new(), |mut wires, (inner, outer)| {
                        wires.insert(inner, outer);
                        wires
                    });
                Part { name, wires }
            },
        ))
    };

    //Call parse with the input to execute the parser
    let result = part().easy_parse(&buffer[..]);
    match result {
        Ok((value, _remaining_input)) => Ok(Some(value)),
        Err(err) => Err(format!("{}", err)),
    }
}
