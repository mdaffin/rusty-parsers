use pest::{Parser, iterators::Pairs};
use super::Part;
use std::collections::HashMap;

#[cfg(debug_assertions)]
const _GRAMMAR: &'static str = include_str!("part.pest");

#[derive(Parser)]
#[grammar = "part.pest"]
struct PartParser;

pub fn parse(buffer: &str) -> Result<Option<Part>, String> {
    // Pest is not astreaming parser, so we cannot tell the difference between incorrect input and
    // incomplete input. We need to detect this ourselves and for the chosen grammer this is
    // reasonalby easy, if the last none whitespace char is ";" we have a complete input. This is
    // not ideal and there are multiple edge cases but it will do for this demo.
    if !buffer.trim_right().ends_with(";") {
        return Ok(None);
    }

    let pairs = PartParser::parse(Rule::part, buffer).map_err(|e| format!("{}", e))?;
    let part = pair_to_part(pairs).unwrap();
    Ok(Some(part))
}

// Takes a pair, pops the next value and returns the string contents after asserting it matches the
// given rule. This will cause a panic if there are no pairs left or the rule does not match.
macro_rules! decode_next {
    ($pairs:expr, $rule:expr) => {{
        let p = $pairs.next().unwrap();
        assert_eq!(p.as_rule(), $rule);
        p.into_span().as_str()
    }};
}

fn pair_to_part(mut pairs: Pairs<Rule>) -> Result<Part, String> {
    // Unwrap the outer pair decending into it
    let mut pairs = {
        let pair = pairs.next().expect("missing part");
        assert_eq!(pair.as_rule(), Rule::part);
        assert_eq!(
            pairs.next(),
            None,
            "there should be only one part but extra tokens where found"
        );
        pair.into_inner()
    };

    let name = decode_next!(pairs, Rule::part_name);

    let wires = pairs.fold(HashMap::new(), |mut map, pair| {
        assert_eq!(pair.as_rule(), Rule::wire_pair);
        let (inner, outer) = pair_to_wire_link(pair.into_inner());
        map.insert(inner, outer);
        map
    });

    Ok(Part { name, wires })
}

fn pair_to_wire_link(mut pairs: Pairs<Rule>) -> (&str, &str) {
    let inner = decode_next!(pairs, Rule::inner_wire_name);
    let outer = decode_next!(pairs, Rule::outer_wire_name);
    assert_eq!(pairs.next(), None);
    (inner, outer)
}
