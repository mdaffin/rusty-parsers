extern crate rusty_parser;
extern crate rustyline;

use rustyline::error::ReadlineError;
use rustyline::Editor;

use rusty_parser::parse;

fn main() {
    let mut buffer = String::new();
    let mut rl = Editor::<()>::new();

    loop {
        let prompt = if buffer.is_empty() { ">>> " } else { "... " };
        let line = match rl.readline(prompt) {
            Ok(line) => line,
            Err(ReadlineError::Eof) | Err(ReadlineError::Interrupted) => break,
            // For other errors print them and exit with an error. This should not happen often and
            // means there is a problem with reading from stdin or the chars read are not UTF8.
            Err(e) => {
                eprintln!("Could not read from stdin: {}", e);
                ::std::process::exit(1);
            }
        };

        buffer.push_str(&line);
        // Input can span multiple lines and the grammer can deal with this but readline above
        // strips the trailing new line, so we add it back
        buffer.push('\n');

        match buffer.trim() {
            // No input, such as if the user just hits enter
            "" => {
                buffer.clear();
                continue;
            }
            "exit" => break,
            _ => (),
        }

        match parse(buffer.as_str()) {
            Ok(None) => continue, // Need more input
            Ok(Some(result)) => println!("{:?}", result),
            Err(msg) => eprintln!("{}", msg),
        }

        // Add the buffer to the history without the last newline, we do this for any complete
        // input, even if it is invalid so the user can correct any mistake they make.
        rl.add_history_entry(buffer.trim_right_matches('\n').to_string());
        buffer.clear();
    }
}
