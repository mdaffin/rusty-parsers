extern crate combine;
#[macro_use]
extern crate lalrpop_util;
#[macro_use]
extern crate nom;
extern crate pest;
#[macro_use]
extern crate pest_derive;

use std::collections::HashMap;

#[cfg(feature = "lib_lalrpop")]
mod lib_lalrpop;
#[cfg(feature = "lib_lalrpop")]
pub use lib_lalrpop::parse;

#[cfg(feature = "lib_pest")]
mod lib_pest;
#[cfg(feature = "lib_pest")]
pub use lib_pest::parse;

#[cfg(feature = "lib_nom")]
mod lib_nom;
#[cfg(feature = "lib_nom")]
pub use lib_nom::parse;

#[cfg(feature = "lib_combine")]
mod lib_combine;
#[cfg(feature = "lib_combine")]
pub use lib_combine::parse;

#[cfg(test)]
mod tests;

#[derive(Debug, PartialEq)]
pub struct Part<'a> {
    pub name: &'a str,
    pub wires: HashMap<&'a str, &'a str>,
}
