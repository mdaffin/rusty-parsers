lalrpop_mod!(pub part);

use super::Part;

pub fn parse(buffer: &str) -> Result<Option<Part>, String> {
    // Lalrpop is not astreaming parser, so we cannot tell the difference between incorrect input
    // and incomplete input. We need to detect this ourselves and for the chosen grammer this is
    // reasonalby easy, if the last none whitespace char is ";" we have a complete input. This is
    // not ideal and there are multiple edge cases but it will do for this demo.
    if !buffer.trim_right().ends_with(";") {
        Ok(None)
    } else {
        part::PartParser::new()
            .parse(buffer)
            .map(|p| Some(p))
            .map_err(|e| e.to_string())
    }
}
